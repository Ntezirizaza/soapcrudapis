package rw.ac.rca.soapcrudapis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rw.ac.rca.soapcrudapis.models.Supplier;

@Repository
public interface ISupplierRepository extends JpaRepository<Supplier, Long> {

}
