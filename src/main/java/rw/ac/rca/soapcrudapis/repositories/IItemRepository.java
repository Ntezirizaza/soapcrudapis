package rw.ac.rca.soapcrudapis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rw.ac.rca.soapcrudapis.models.Item;

@Repository
public interface IItemRepository extends JpaRepository<Item, Long> {

}
