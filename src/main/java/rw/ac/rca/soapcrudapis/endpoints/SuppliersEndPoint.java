package rw.ac.rca.soapcrudapis.endpoints;

import jaxb.classes.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import rw.ac.rca.soapcrudapis.repositories.ISupplierRepository;
import rw.ac.rca.soapcrudapis.models.Supplier;

import java.util.List;
import java.util.Optional;

@Endpoint
public class SuppliersEndPoint {
    private final ISupplierRepository supplierRepository;

    @Autowired
    public SuppliersEndPoint(ISupplierRepository repository) {
        this.supplierRepository = repository;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/erneste/stocks", localPart = "NewSupplierDTORequest")
    @ResponsePayload
    public NewSupplierDTOResponse create(@RequestPayload NewSupplierDTORequest dto) {
        jaxb.classes.Supplier __supplier = dto.getSupplier();

        Supplier _supplier = new Supplier(__supplier.getNames(), __supplier.getEmail(), __supplier.getMobilePhone());

        Supplier supplier = supplierRepository.save(_supplier);

        NewSupplierDTOResponse response = new NewSupplierDTOResponse();

        __supplier.setId(supplier.getId());

        response.setSupplier(__supplier);

        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/erneste/stocks", localPart = "GetAllSuppliersRequest")
    @ResponsePayload
    public GetAllSuppliersResponse findAll(@RequestPayload GetAllSuppliersRequest request){

        List<Supplier> suppliers = supplierRepository.findAll();

        GetAllSuppliersResponse response = new GetAllSuppliersResponse();

        for (Supplier supplier: suppliers){
            jaxb.classes.Supplier _supplier = mapSupplier(supplier);

            response.getSupplier().add(_supplier);
        }

        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/erneste/stocks", localPart = "GetSupplierDetailsRequest")
    @ResponsePayload
    public GetSupplierDetailsResponse findById(@RequestPayload GetSupplierDetailsRequest request){
        Optional<Supplier> _supplier = supplierRepository.findById(request.getId());

        if(!_supplier.isPresent())
            return new GetSupplierDetailsResponse();

        Supplier supplier = _supplier.get();

        GetSupplierDetailsResponse response = new GetSupplierDetailsResponse();

        jaxb.classes.Supplier __supplier = mapSupplier(supplier);

       response.setSupplier(__supplier);

       return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/erneste/stocks", localPart = "DeleteSupplierRequest")
    @ResponsePayload
    public DeleteSupplierResponse delete(@RequestPayload DeleteSupplierRequest request){
        supplierRepository.deleteById(request.getId());
        DeleteSupplierResponse response = new DeleteSupplierResponse();
        response.setMessage("Successfully deleted a message");
        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/erneste/stocks", localPart = "UpdateSupplierRequest")
    @ResponsePayload
    public UpdateSupplierResponse update(@RequestPayload UpdateSupplierRequest request){
        jaxb.classes.Supplier __supplier = request.getSupplier();

        Supplier _supplier = new Supplier(__supplier.getNames(), __supplier.getEmail(), __supplier.getMobilePhone());
        _supplier.setId(__supplier.getId());

        Supplier supplier = supplierRepository.save(_supplier);

        UpdateSupplierResponse supplierDTO = new UpdateSupplierResponse();

        __supplier.setId(supplier.getId());

        supplierDTO.setSupplier(__supplier);

        return supplierDTO;
    }

    private jaxb.classes.Supplier mapSupplier(Supplier supplier){
        jaxb.classes.Supplier _supplier = new jaxb.classes.Supplier();
        _supplier.setId(supplier.getId());
        _supplier.setNames(supplier.getNames());
        _supplier.setEmail(supplier.getEmail());
        _supplier.setMobilePhone(supplier.getMobilePhone());

        return _supplier;
    }
}
