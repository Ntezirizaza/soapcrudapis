package rw.ac.rca.soapcrudapis.endpoints;

import jaxb.classes.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import rw.ac.rca.soapcrudapis.models.enums.StatusType;
import rw.ac.rca.soapcrudapis.repositories.IItemRepository;
import rw.ac.rca.soapcrudapis.models.Item;

import java.util.List;
import java.util.Optional;

@Endpoint
public class ItemsEndPoint {
    private final IItemRepository itemRepository;

    @Autowired
    public ItemsEndPoint(IItemRepository repository) {
        this.itemRepository = repository;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/erneste/stocks", localPart = "NewItemDTORequest")
    @ResponsePayload
    public NewItemDTOResponse create(@RequestPayload NewItemDTORequest dto) {
        jaxb.classes.Item __item = dto.getItem();

        Item _item = new Item(__item.getName(), __item.getItemCode(), __item.getStatus(), __item.getPrice(), __item.getSupplier());
        Item item = itemRepository.save(_item);

        NewItemDTOResponse response = new NewItemDTOResponse();

        __item.setId(item.getId());

        response.setItem(__item);

        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/erneste/stocks", localPart = "GetAllItemsRequest")
    @ResponsePayload
    public GetAllItemsResponse findAll(@RequestPayload GetAllItemsRequest request){

        List<Item> items = itemRepository.findAll();

        GetAllItemsResponse response = new GetAllItemsResponse();

        for (Item item : items){
            jaxb.classes.Item _item = mapItem(item);

            response.getItem().add(_item);
        }

        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/erneste/stocks", localPart = "GetItemDetailsRequest")
    @ResponsePayload
    public GetItemDetailsResponse findById(@RequestPayload GetItemDetailsRequest request){
        Optional<Item> _item = itemRepository.findById(request.getId());

        if(!_item.isPresent())
            return new GetItemDetailsResponse();

        Item item = _item.get();

        GetItemDetailsResponse response = new GetItemDetailsResponse();

        jaxb.classes.Item __item = mapItem(item);

        response.setItem(__item);

        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/erneste/stocks", localPart = "DeleteCourseRequest")
    @ResponsePayload
    public DeleteItemResponse delete(@RequestPayload DeleteItemRequest request){
        itemRepository.deleteById(request.getId());
        DeleteItemResponse response = new DeleteItemResponse();
        response.setMessage("Successfully deleted a message");
        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/erneste/stocks", localPart = "UpdateItemRequest")
    @ResponsePayload
    public UpdateItemResponse update(@RequestPayload UpdateItemRequest request){
        jaxb.classes.Item __item = request.getItem();

        Item _item = new Item(__item.getName(), __item.getItemCode() , __item.getStatus(),__item.getPrice(), __item.getSupplier());
        _item.setId(__item.getId());

        Item item = itemRepository.save(_item);

        UpdateItemResponse itemDTO = new UpdateItemResponse();

        __item.setId(item.getId());

        itemDTO.setItem(__item);

        return itemDTO;
    }

    private jaxb.classes.Item mapItem(Item item){
        jaxb.classes.Item _item = new jaxb.classes.Item();
        _item.setId(item.getId());
        _item.setName(item.getName());
        _item.setItemCode(item.getItemCode());
        _item.setStatus(StatusType.NEW);
        _item.setPrice(item.getPrice());
        _item.setSupplier(item.getSupplier());

        return _item;
    }
}
