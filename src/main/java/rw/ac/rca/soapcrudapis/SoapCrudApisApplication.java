package rw.ac.rca.soapcrudapis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoapCrudApisApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoapCrudApisApplication.class, args);
	}

}
