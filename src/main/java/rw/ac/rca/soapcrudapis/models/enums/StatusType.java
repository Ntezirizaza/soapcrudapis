package rw.ac.rca.soapcrudapis.models.enums;

public enum StatusType {
    NEW, GOOD_SHAPE, OLD
}
