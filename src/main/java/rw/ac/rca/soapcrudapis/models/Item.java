package rw.ac.rca.soapcrudapis.models;

import javax.persistence.*;

import rw.ac.rca.soapcrudapis.models.enums.StatusType;

@Entity
@Table(name = "items")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    public String name;

    public String itemCode;

    @Enumerated(EnumType.STRING)
    private StatusType statusType;

    public String price;

    @ManyToOne
    private Supplier supplier;

    public Item(String name, String itemCode, StatusType statusType, String price, Supplier supplier) {
        this.name = name;
        this.itemCode = itemCode;
        this.statusType = statusType;
        this.price = price;
        this.supplier=supplier;
    }

    public Item() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public StatusType getStatus() {
        return statusType;
    }

    public void setStatus(StatusType statusType) {
        this.statusType = statusType;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }
}
